#pragma once
#include<unordered_map>
#include "Piece.h"
#include <string>
#include<sstream>
class UnusedPieces
{
	std::unordered_map<std::string, Piece> m_unusedPieces;
	void Initialize();
	void Emplace(const Piece&);
public:
	UnusedPieces();
	Piece choosePiece(const std::string&);
	friend std::ostream& operator<<(std::ostream&, const UnusedPieces&);
};

