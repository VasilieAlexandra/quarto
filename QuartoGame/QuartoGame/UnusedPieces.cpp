#include "UnusedPieces.h"

void UnusedPieces::Initialize()
{

	this->Emplace(Piece(Piece::Colour::White, Piece::Shape::Round, Piece::Height::Short, Piece::BodyShape::Hollow));
	this->Emplace(Piece(Piece::Colour::White, Piece::Shape::Round, Piece::Height::Short, Piece::BodyShape::Full));
	this->Emplace(Piece(Piece::Colour::White, Piece::Shape::Round, Piece::Height::Tall, Piece::BodyShape::Hollow));
	this->Emplace(Piece(Piece::Colour::White, Piece::Shape::Round, Piece::Height::Tall, Piece::BodyShape::Full));
	
	this->Emplace(Piece(Piece::Colour::White, Piece::Shape::Square, Piece::Height::Short, Piece::BodyShape::Hollow));
	this->Emplace(Piece(Piece::Colour::White, Piece::Shape::Square, Piece::Height::Short, Piece::BodyShape::Full));
	this->Emplace(Piece(Piece::Colour::White, Piece::Shape::Square, Piece::Height::Tall, Piece::BodyShape::Hollow));
	this->Emplace(Piece(Piece::Colour::White, Piece::Shape::Square, Piece::Height::Tall, Piece::BodyShape::Full));
	
	this->Emplace(Piece(Piece::Colour::Black, Piece::Shape::Round, Piece::Height::Short, Piece::BodyShape::Hollow));
	this->Emplace(Piece(Piece::Colour::Black, Piece::Shape::Round, Piece::Height::Short, Piece::BodyShape::Full));
	this->Emplace(Piece(Piece::Colour::Black, Piece::Shape::Round, Piece::Height::Tall, Piece::BodyShape::Hollow));
	this->Emplace(Piece(Piece::Colour::Black, Piece::Shape::Round, Piece::Height::Tall, Piece::BodyShape::Full));


	this->Emplace(Piece(Piece::Colour::Black, Piece::Shape::Square, Piece::Height::Short, Piece::BodyShape::Hollow));
	this->Emplace(Piece(Piece::Colour::Black, Piece::Shape::Square, Piece::Height::Short, Piece::BodyShape::Full));
	this->Emplace(Piece(Piece::Colour::Black, Piece::Shape::Square, Piece::Height::Tall, Piece::BodyShape::Hollow));
	this->Emplace(Piece(Piece::Colour::Black, Piece::Shape::Square, Piece::Height::Tall, Piece::BodyShape::Full));
	
}

void UnusedPieces::Emplace(const Piece& piece)
{
	std::stringstream ss;
	ss << piece;
	m_unusedPieces.insert(std::make_pair(ss.str(), piece));
}

UnusedPieces::UnusedPieces()
{
	Initialize();
}

Piece UnusedPieces::choosePiece(const std::string& key)
{
	auto extracted = m_unusedPieces.extract(key);
	if (extracted)
		return std::move(extracted.mapped());
	else 
		throw "Piece not found. \n";
}

std::ostream& operator<<(std::ostream& fl, const UnusedPieces& up)
{
	for (auto unusedPiece : up.m_unusedPieces)
		fl << unusedPiece.first << " " ;
	fl << "\n";
	return fl;
}
