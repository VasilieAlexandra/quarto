#include "Piece.h"

Piece::Piece(Colour c, Shape s, Height h, BodyShape b)
	:mColour(c),mShape(s),mHeight(h),mBodyShape(b)
{
}

Piece::Colour Piece::GetColour()const
{
	return mColour;
}

Piece::Shape Piece::GetShape()const
{
	return mShape;
}

Piece::Height Piece::GetHeight()const
{
	return mHeight;
}

Piece::BodyShape Piece::GetBodyShape()const
{
	return mBodyShape;
}

std::ostream& operator<<(std::ostream& fl, const Piece& piece)
{
	
	return fl<<static_cast<int>(piece.mColour)<<static_cast<int>(piece.mShape)
		<< static_cast<int>(piece.mHeight)
		<< static_cast<int>( piece.mBodyShape );
}
