#pragma once
#include<array>
#include"Piece.h"
#include"optional"
class Board
{


private:
	std::array<std::optional<Piece>, 16> m_board;//pt a avea pozitii in care nu exista piese

public:
	using Position = std::pair<uint8_t, uint8_t>;
	const std::optional<Piece>& operator[](const Position&)const;
	std::optional<Piece>& operator[](const Position&);
	friend std::ostream& operator<<(std::ostream&,const Board&);

};

