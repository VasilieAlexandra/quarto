#include "QuartoGame.h"


void QuartoGame::Run()
{
	Board board;
	std::string name;
	std::cout << "Introduceti numele pentru player1: ";
	std::cin >> name;
	Player player1(name);
	std::cout << "Introduceti numele pentru player2: ";
	std::cin >> name;
	Player player2(name);
	UnusedPieces unused_pieces;
	std::reference_wrapper<Player> placeing_player=player1;
	std::reference_wrapper<Player> choosing_player=player2;


	while (true)
	{
		system("cls");
		std::cout << board;
		std::cout << std::endl;
		
		std::cout << "Piese disponibile:\n";
		std::cout << unused_pieces;
		std::cout<<std:: endl;
		std::cout << choosing_player << "trebuie sa alegem o piesa";
		Piece piece;
		try 
		{
			std::string piece_name;
			std::cin >> piece_name;
			 piece = unused_pieces.choosePiece(piece_name);
		}
		catch (const char* mesaj) 
		{
			std::cout << mesaj;
		}

		int line, col;
		std::cout << placeing_player << "trebuie sa aleaga o pozitie";
		
		std::cin >> line >> col;
		try 
		{
			board[{line, col}] = std::move(piece);
		}
		catch (const char* mesaj)
		{
			std::cout << mesaj;
		}
		std::swap(placeing_player, choosing_player);


	}



}

