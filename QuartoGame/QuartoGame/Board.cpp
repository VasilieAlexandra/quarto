#include "Board.h"
#include<tuple>
const std::optional<Piece>& Board::operator[](const Position& positionReceived) const
{
	//uint8_t line=positionReceived.first, column= positionReceived.second;
	//std::tie(line, column) = positionReceived;
	const auto& [line, column] = positionReceived;
	return m_board[4 * line + column];

}

std::optional<Piece>& Board::operator[](const Position& positionReceived)
{
	
	//uint8_t line = positionReceived.first, column= positionReceived.second;
	//std::tie(line, column) = positionReceived;
	const auto& [line, column] = positionReceived;
	if (line >= 4 || line < 0 || column >= 4 || column < 0)
		throw"Pozitie invalida \n";
	return m_board[4*line+column];

}

std::ostream& operator<<(std::ostream& fl, const Board& m_board )
{
	Board::Position position;
	//uint8_t line = position.first, column = position.second;
	auto& [line, column] = position;
	for (line = 0; line < 4; line++)
	{
		for (column = 0; column < 4; column++)
		{   
		    if(m_board[position])
			  fl << *m_board[position]<<" ";
		    else 
			  fl << "----" << " ";

		}
		
		fl << std::endl;
	}
	return fl;

}
