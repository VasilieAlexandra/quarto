#pragma once
#include<iostream>
class Piece
{

public:
	enum class Colour: uint8_t
	{
		Black,White
	};

    enum class Shape : uint8_t
	{
		Round, Square
	};
	enum class Height : uint8_t
	{
		Short, Tall
	};	
	enum class BodyShape : uint8_t
	{
		Hollow,Full
	};

private:

	Colour mColour:1;//foloseste un singur bit
	Shape mShape:1;
	Height mHeight:1;
	BodyShape mBodyShape:1;
public:
	Piece() = default;
	Piece(Colour c, Shape s, Height h, BodyShape b);
	Colour GetColour()const;
	Shape GetShape()const;
	Height GetHeight()const;
	BodyShape GetBodyShape()const;
	friend std::ostream& operator<<(std::ostream& fl, const Piece& piece);

};

