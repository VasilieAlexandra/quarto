#pragma once
#include<iostream>
#include<string>
#ifdef LOGGING_EXPORTS
#define LOGGING_API _declspec(dllexport)
#else
#define LOGGING_API _declspec(dllimport)
#endif


class LOGGING_API Logger
{

public:
	enum class Level
	{
		Info,
		Warnning,
		Error
	
	};
	Logger(std::ostream& outstream, Level lvl = Level::Info);
	void Log(const std::string& message,Level level);

  private:
	    Level m_level;
		std::ostream& outstream;
};

