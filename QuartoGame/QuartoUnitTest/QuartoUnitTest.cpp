#include "pch.h"
#include "CppUnitTest.h"
#include"../QuartoGame/Piece.h"
#include"../QuartoGame/Piece.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuartoUnitTest
{
	TEST_CLASS(QuartoUnitTest)
	{
	public:
		
		TEST_METHOD(PieceConstructor)
		{
			Piece piece(Piece::Colour::Black, Piece::Shape::Square, Piece::Height::Short, Piece::BodyShape::Hollow);
			Assert::IsTrue(Piece::BodyShape::Hollow == piece.GetBodyShape());
			Assert::IsTrue(Piece::Colour::Black== piece.GetColour());
		}
	};
}
