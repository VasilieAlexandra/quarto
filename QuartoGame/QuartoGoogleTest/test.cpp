#include "pch.h"
#include"../QuartoGame/Piece.h"
#include"../QuartoGame/Piece.cpp"

TEST(TestCaseName, TestName) 
{
   Piece piece(Piece::Colour::Black, Piece::Shape::Square, Piece::Height::Short, Piece::BodyShape::Hollow);
  EXPECT_EQ(Piece::BodyShape::Hollow ,piece.GetBodyShape());
  EXPECT_TRUE(Piece::Colour::Black == piece.GetColour());
}